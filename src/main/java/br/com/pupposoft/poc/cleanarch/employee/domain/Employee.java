package br.com.pupposoft.poc.cleanarch.employee.domain;

import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Employee {
	private String cpf;
	private String firstName;
	private String lastName;
	private LocalDate birthDate;
	
	private List<Salary> salarys;
}
