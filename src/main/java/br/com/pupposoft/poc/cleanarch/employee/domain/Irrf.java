package br.com.pupposoft.poc.cleanarch.employee.domain;

import java.math.BigDecimal;
import java.util.List;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/* Foi implementado um cálculo mais simples para descontar apenas o IRRF, ou seja, 
 * não está contando que haja dependentes ou qualquer outra dedução.
 */

@Slf4j
@Getter
@ToString
public class Irrf extends AliquotDiscount {
	private BigDecimal dependentValue;
	private Integer dependents;
	private BigDecimal otherDeductions;
	private AliquotDiscount inss;

	public Irrf(
			String name, 
			BigDecimal dependentValue,
			Integer dependents, 
			BigDecimal otherDeductions,
			List<Tariff> tariffTable, 
			AliquotDiscount inss) {

		super(name, tariffTable);
		this.dependentValue = dependentValue;
		this.dependents = dependents;
		this.otherDeductions = otherDeductions;
		this.inss = inss;		
	}

	@Override
	public BigDecimal calculate(BigDecimal grossSalary) {
		//TODO falta implementar o cálculo de desconto de dependentes e outras deduções.  
		
		log.trace("Start grossSalary={}", grossSalary);

		final BigDecimal inssDiscount = this.inss.calculate(grossSalary);
		BigDecimal calculationBasis = grossSalary.add(inssDiscount);
		BigDecimal discountIrrf = super.calculate(calculationBasis);

		log.trace("End discountIrrf={}", discountIrrf);
		return discountIrrf;
	}

}
