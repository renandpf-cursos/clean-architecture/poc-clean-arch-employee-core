package br.com.pupposoft.poc.cleanarch.employee.usecase;

import java.util.Optional;

import br.com.pupposoft.poc.cleanarch.employee.domain.Employee;
import br.com.pupposoft.poc.cleanarch.employee.exception.EmployeeAlreadyExistsException;
import br.com.pupposoft.poc.cleanarch.employee.gateway.DataBaseGateway;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ValidateCreateUpdateUseCase {

	private DataBaseGateway<String> dataBaseGateway;
	
	protected void validate(final String cpf) {
		log.trace("Start cpf={}", cpf);
		final Optional<Employee> employeeOp = dataBaseGateway.findByCpf(cpf);
		if(employeeOp.isPresent()) {
			log.warn("Employee already exists. cpf={}", cpf);
			throw new EmployeeAlreadyExistsException();
		}
		log.trace("End");
	}
}
