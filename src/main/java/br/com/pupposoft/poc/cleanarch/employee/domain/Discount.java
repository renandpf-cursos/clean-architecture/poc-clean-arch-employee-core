package br.com.pupposoft.poc.cleanarch.employee.domain;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Discount {
	private String name;
	private BigDecimal percent;
	
	public Discount(final String name) {
		this.name = name;
	}
	
	public BigDecimal calculate(BigDecimal grossSalary) {
		log.trace("Start grossSalary={}", grossSalary);
		BigDecimal discountValue = calculateSimpleDiscount(grossSalary);
		log.trace("End discountValue={}", discountValue);
		return discountValue;
	}

	private BigDecimal calculateSimpleDiscount(BigDecimal grossSalary) {
		BigDecimal percentage = getPercentage();
		return grossSalary.multiply(percentage).negate();
	}

	private BigDecimal getPercentage() {
		return this.percent.divide(new BigDecimal("100"));
	}
}
