package br.com.pupposoft.poc.cleanarch.employee.domain;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class Tariff {
	private BigDecimal maxRange;
	private BigDecimal percent;
	
	public boolean isInfiniteRange() {
		return maxRange == null;
	}

}
