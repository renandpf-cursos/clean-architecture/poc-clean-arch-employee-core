package br.com.pupposoft.poc.cleanarch.employee.domain;

import java.math.BigDecimal;
import java.util.List;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@ToString
public class AliquotDiscount extends Discount{
	private List<Tariff> tariffTable;

	public AliquotDiscount(String name, List<Tariff> tariffTable) {
		super(name);
		this.tariffTable = tariffTable;
	}
	
	@Override
	public BigDecimal calculate(BigDecimal grossSalary) {
		log.trace("Start grossSalary={}", grossSalary);
		
		BigDecimal result = new BigDecimal("0.000");
		BigDecimal rangeToCalculate = new BigDecimal("0.000");
		BigDecimal previousMaxRange = new BigDecimal("0.000");
		
		for (Tariff tariff : tariffTable) {
			BigDecimal percentage = getPercentage(tariff);
			if(tariff.isInfiniteRange() || grossSalary.compareTo(tariff.getMaxRange()) <= 0 ) {
				rangeToCalculate = grossSalary.subtract(previousMaxRange);
				result = getResult(result, rangeToCalculate, percentage);
				break;
			} 
			rangeToCalculate = tariff.getMaxRange().subtract(previousMaxRange);
			result = getResult(result, rangeToCalculate, percentage);
			previousMaxRange = tariff.getMaxRange();
		}
		log.trace("End result={}", result);
		return result;
	}

	private BigDecimal getPercentage(Tariff tariff) {
		return tariff.getPercent().divide(new BigDecimal("100"));
	}
	
	private BigDecimal getResult(BigDecimal result, BigDecimal rangeToCalculate, BigDecimal percentage) {
		return (rangeToCalculate.multiply(percentage).negate()).add(result);
	}
	
}
