package br.com.pupposoft.poc.cleanarch.employee.gateway;

import java.util.List;
import java.util.Optional;

import br.com.pupposoft.poc.cleanarch.employee.domain.Employee;

public interface DataBaseGateway<T> {

	T save(Employee employeeToCreate);

	Optional<Employee> findById(T id);

	void delete(T id);

	Optional<Employee> findByCpf(String cpf);

	List<Employee> getAll();
}
