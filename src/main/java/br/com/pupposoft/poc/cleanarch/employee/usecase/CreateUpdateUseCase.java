package br.com.pupposoft.poc.cleanarch.employee.usecase;

import br.com.pupposoft.poc.cleanarch.employee.domain.Employee;
import br.com.pupposoft.poc.cleanarch.employee.gateway.DataBaseGateway;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class CreateUpdateUseCase<T> {

	private ValidateCreateUpdateUseCase validateCreateUpdateUseCase;
	private DataBaseGateway<T> dataBaseGateway;
	
	public T create(final Employee employee) {
		log.trace("Start employee={}", employee);
		validateCreateUpdateUseCase.validate(employee.getCpf());
		T idEmployeeCreated = dataBaseGateway.save(employee);
		log.trace("End employeeSaved={}", idEmployeeCreated);
		return idEmployeeCreated;
	}
}
