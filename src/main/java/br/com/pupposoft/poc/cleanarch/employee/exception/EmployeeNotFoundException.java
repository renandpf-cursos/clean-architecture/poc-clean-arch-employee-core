package br.com.pupposoft.poc.cleanarch.employee.exception;

import lombok.Getter;

@Getter
public class EmployeeNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -2576624572494430601L;

	private final String code = "poc.cleanarch.employee.notFound";
	private final String message = "Employee not found";
}
