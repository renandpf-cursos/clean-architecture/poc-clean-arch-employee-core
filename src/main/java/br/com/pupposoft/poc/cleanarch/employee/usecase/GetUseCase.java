package br.com.pupposoft.poc.cleanarch.employee.usecase;

import java.util.List;

import br.com.pupposoft.poc.cleanarch.employee.domain.Employee;
import br.com.pupposoft.poc.cleanarch.employee.exception.EmployeeNotFoundException;
import br.com.pupposoft.poc.cleanarch.employee.gateway.DataBaseGateway;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GetUseCase<T> {

	private DataBaseGateway<T> dataBaseGateway;

	public Employee get(T id) {
		log.trace("Start id={}", id);
		Employee employeeToReturn = dataBaseGateway
				.findById(id)
				.orElseThrow(() -> {
					log.warn("Employee not found. id={}", id);
					return new EmployeeNotFoundException();
				});
		
		log.trace("End employeeToReturn={}", employeeToReturn);
		return employeeToReturn;
	}

	public List<Employee> getAll() {
		log.trace("Start");
		List<Employee> employeesToReturn = dataBaseGateway.getAll();
		log.trace("End employeesToReturn={}", employeesToReturn);
		return employeesToReturn;
	}

}
