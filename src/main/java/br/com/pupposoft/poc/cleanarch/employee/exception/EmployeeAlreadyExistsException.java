package br.com.pupposoft.poc.cleanarch.employee.exception;

import lombok.Getter;

@Getter
public class EmployeeAlreadyExistsException extends RuntimeException {
	private static final long serialVersionUID = -8218132413030974490L;
	private final String code = "poc.cleanarch.employee.alreadyExists";
	private final String message = "Employee already exists";
}
