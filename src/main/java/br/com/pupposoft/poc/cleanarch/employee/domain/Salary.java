package br.com.pupposoft.poc.cleanarch.employee.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Salary {
	private LocalDate date;
	private BigDecimal grossSalary;
	private List<Discount> discounts;
	
	public BigDecimal getNetSalary(BigDecimal grossSalary) {
		log.trace("Start grossSalary={}", grossSalary);
		BigDecimal somaDiscunt = new BigDecimal("0.000");
		for (Discount discount : discounts) {
			somaDiscunt = somaDiscunt.add(discount.calculate(grossSalary));
		}
		BigDecimal netSalary = grossSalary.add(somaDiscunt); 
		log.trace("End netSalary={}", netSalary);
		return netSalary;
	}
	
}
