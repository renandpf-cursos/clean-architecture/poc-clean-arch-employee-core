package br.com.pupposoft.poc.cleanarch.employee.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class IrrfUnitTest {
	
	@Test
	public void range1Exempt() {
		final BigDecimal grossSalary = new BigDecimal("1900.00");
		final String name = "IRRF";
		final BigDecimal dependentValue = new BigDecimal("189.59");
		
		final AliquotDiscount inss = new AliquotDiscount("INSS", getInssTariffTable());
		
		final Discount irrf = new Irrf(name, dependentValue, null, null, getIrrfTariffTable(), inss);
		
		BigDecimal result =  irrf.calculate(grossSalary);
		
		assertEquals(new BigDecimal("0.000"), result.setScale(3));
	}
	
	@Test
	public void range2() {
		final BigDecimal grossSalary = new BigDecimal("3000.00");
		final String name = "IRRF";
		final BigDecimal dependentValue = new BigDecimal("189.59");
		
		final AliquotDiscount inss = new AliquotDiscount("INSS", getInssTariffTable());
		
		final Discount irrf = new Irrf(name, dependentValue, null, null, getIrrfTariffTable(), inss);
		
		BigDecimal result =  irrf.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-61.396830000"), result);
	}
	
	@Test
	public void range3() {
		final BigDecimal grossSalary = new BigDecimal("3800.00");
		final String name = "IRRF";
		final BigDecimal dependentValue = new BigDecimal("189.59");
		
		final AliquotDiscount inss = new AliquotDiscount("INSS", getInssTariffTable());
		
		final Discount irrf = new Irrf(name, dependentValue, null, null, getIrrfTariffTable(), inss);
		
		BigDecimal result =  irrf.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-157.70682000"), result);
	}
	
	@Test
	public void range4() {
		final BigDecimal grossSalary = new BigDecimal("4600.00");
		final String name = "IRRF";
		final BigDecimal dependentValue = new BigDecimal("189.59");
		
		final AliquotDiscount inss = new AliquotDiscount("INSS", getInssTariffTable());
		
		final Discount irrf = new Irrf(name, dependentValue, null, null, getIrrfTariffTable(), inss);
		
		BigDecimal result =  irrf.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-287.431230000"), result);
	}
	
	@Test
	public void range5() {
		final BigDecimal grossSalary = new BigDecimal("5486.08");
		final String name = "IRRF";
		final BigDecimal dependentValue = new BigDecimal("189.59");
		
		final AliquotDiscount inss = new AliquotDiscount("INSS", getInssTariffTable());
		
		final Discount irrf = new Irrf(name, dependentValue, null, null, getIrrfTariffTable(), inss);
		
		BigDecimal result =  irrf.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-468.990590000"), result);
	}
	
	private List<Tariff> getInssTariffTable() {
		final List<Tariff> inss = new ArrayList<>();
		inss.add(new Tariff(new BigDecimal("1100.00"), new BigDecimal("7.5")));
		inss.add(new Tariff(new BigDecimal("2203.48"), new BigDecimal("9")));
		inss.add(new Tariff(new BigDecimal("3305.22"), new BigDecimal("12")));
		inss.add(new Tariff(new BigDecimal("6433.57"), new BigDecimal("14")));
		return inss;
	}
	
	private List<Tariff> getIrrfTariffTable() {
		final List<Tariff> irrf = new ArrayList<>();
		irrf.add(new Tariff(new BigDecimal("1903.98"), new BigDecimal("0.000")));
		irrf.add(new Tariff(new BigDecimal("2826.68"), new BigDecimal("7.5")));
		irrf.add(new Tariff(new BigDecimal("3751.05"), new BigDecimal("15")));
		irrf.add(new Tariff(new BigDecimal("4664.68"), new BigDecimal("22.5")));
		irrf.add(new Tariff(null, new BigDecimal("27.5")));
		return irrf;
	}

}
