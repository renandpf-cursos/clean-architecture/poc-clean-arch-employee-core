package br.com.pupposoft.poc.cleanarch.employee.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.pupposoft.poc.cleanarch.employee.domain.Employee;
import br.com.pupposoft.poc.cleanarch.employee.gateway.DataBaseGateway;
import br.com.pupposoft.poc.test.util.databuilder.domain.EmployeeDataBuilder;

@ExtendWith(MockitoExtension.class)
public class CreateUpdateUseCaseUnitTest {

	@InjectMocks
	private CreateUpdateUseCase<String> createUpdateUseCase;
	
	@Mock
	private ValidateCreateUpdateUseCase validateCreateUpdateUseCase;

	@Mock
	private DataBaseGateway<String> dataBaseGateway;

	@Test
	public void createWithSuccess() {
		final Employee employeeToCreate = EmployeeDataBuilder.getAny();
		final String id = "anyId";
		
		doReturn(id).when(dataBaseGateway).save(employeeToCreate);
		
		final String idCreated = createUpdateUseCase.create(employeeToCreate);
		
		assertEquals(id, idCreated);
		verify(validateCreateUpdateUseCase).validate(employeeToCreate.getCpf());
		verify(dataBaseGateway).save(employeeToCreate);
	}
}
