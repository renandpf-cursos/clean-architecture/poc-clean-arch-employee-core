package br.com.pupposoft.poc.cleanarch.employee.usecase;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.pupposoft.poc.cleanarch.employee.domain.Employee;
import br.com.pupposoft.poc.cleanarch.employee.exception.EmployeeAlreadyExistsException;
import br.com.pupposoft.poc.cleanarch.employee.gateway.DataBaseGateway;
import br.com.pupposoft.poc.test.util.databuilder.domain.EmployeeDataBuilder;

@ExtendWith(MockitoExtension.class)
public class ValidateCreateUpdateUseCaseUnitTest {

	@InjectMocks
	private ValidateCreateUpdateUseCase validateCreateUpdateUseCase;
	
	@Mock
	private DataBaseGateway<String> dataBaseGateway;
	
	@Test
	public void validateWithSuccess() {
		final String cpf = "anyCpf";
		final Optional<Employee> employeeOp = Optional.empty();
		
		doReturn(employeeOp).when(dataBaseGateway).findByCpf(cpf);
		
		validateCreateUpdateUseCase.validate(cpf);
		
		verify(dataBaseGateway).findByCpf(cpf);
	}

	@Test
	public void validateWithError() {
		final String cpf = "anyCpf";
		
		final Employee employeeToBeReturn = EmployeeDataBuilder.getAny();
		
		final Optional<Employee> employeeOp = Optional.of(employeeToBeReturn); 

		doReturn(employeeOp).when(dataBaseGateway).findByCpf(cpf);
		
		assertThrows(EmployeeAlreadyExistsException.class, () -> {
			validateCreateUpdateUseCase.validate(cpf);
		});
	}
}
