package br.com.pupposoft.poc.cleanarch.employee.usecase;

import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.pupposoft.poc.cleanarch.employee.gateway.DataBaseGateway;

@ExtendWith(MockitoExtension.class)
public class DeleteUseCaseUnitTest {

	@InjectMocks
	private DeleteUseCase<String> deleteUseCase;
	
	@Mock
	private DataBaseGateway<String> dataBaseGateway;
	
	@Test
	public void getWithSuccess() {
		final String id = "anyId";
		deleteUseCase.delete(id);
		verify(dataBaseGateway).delete(id);
	}
}
