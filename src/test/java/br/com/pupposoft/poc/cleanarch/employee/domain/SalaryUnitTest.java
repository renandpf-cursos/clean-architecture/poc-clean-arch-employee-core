package br.com.pupposoft.poc.cleanarch.employee.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class SalaryUnitTest {
	
	@Test
	public void netSalarySimpleDiscount() {
		final LocalDate date = LocalDate.now();
		final BigDecimal grossSalary = new BigDecimal("3000");
		final String discountName = "transport";
		final BigDecimal discountPercent = new BigDecimal("6");
		
		final Discount discount = new Discount(discountName, discountPercent);
		final List<Discount> discounts = new ArrayList<>();
		discounts.add(discount);
		
		Salary salary = new Salary(date, grossSalary, discounts);
		
		BigDecimal result = salary.getNetSalary(grossSalary);
		
		assertEquals(new BigDecimal("2820.000"), result);
	}
	
	@Test
	public void netSalaryInssDiscount() {
		final LocalDate date = LocalDate.now();
		final BigDecimal grossSalary = new BigDecimal("3000");
		
		final AliquotDiscount inss = new AliquotDiscount("Inss", getInssTariffTable());
		final List<Discount> discounts = new ArrayList<>();
		discounts.add(inss);
		
		Salary salary = new Salary(date, grossSalary, discounts);
		
		BigDecimal result = salary.getNetSalary(grossSalary);
		
		assertEquals(new BigDecimal("2722.604400"), result);
	}

	@Test
	public void netSalarySimpleDiscountAndInssDiscount() {
		final LocalDate date = LocalDate.now();
		final BigDecimal grossSalary = new BigDecimal("3000");
		final String discountName = "transport";
		final BigDecimal discountPercent = new BigDecimal("6");
		
		final AliquotDiscount inss = new AliquotDiscount("Inss", getInssTariffTable());
		final Discount discount = new Discount(discountName, discountPercent);
		final List<Discount> discounts = new ArrayList<>();
		discounts.add(inss);
		discounts.add(discount);
		
		Salary salary = new Salary(date, grossSalary, discounts);
		
		BigDecimal result = salary.getNetSalary(grossSalary);
		
		assertEquals(new BigDecimal("2542.604400"), result);
	}
	
	@Test
	public void netSalaryIrrfDiscountAndInssDiscount() {
		final LocalDate date = LocalDate.now();
		final BigDecimal grossSalary = new BigDecimal("3000");
		final String name = "IRRF";
		final BigDecimal dependentValue = new BigDecimal("189.59");
		
		final AliquotDiscount inss = new AliquotDiscount("Inss", getInssTariffTable());
		final Discount irrf = new Irrf(name, dependentValue, null, null, getIrrfTariffTable(), inss);
		final List<Discount> discounts = new ArrayList<>();
		discounts.add(inss);
		discounts.add(irrf);
		
		Salary salary = new Salary(date, grossSalary, discounts);
		
		BigDecimal result = salary.getNetSalary(grossSalary);
		
		assertEquals(new BigDecimal("2661.207570000"), result);
	}
	
	private List<Tariff> getInssTariffTable() {
		final List<Tariff> inss = new ArrayList<>();
		inss.add(new Tariff(new BigDecimal("1100.00"), new BigDecimal("7.5")));
		inss.add(new Tariff(new BigDecimal("2203.48"), new BigDecimal("9")));
		inss.add(new Tariff(new BigDecimal("3305.22"), new BigDecimal("12")));
		inss.add(new Tariff(new BigDecimal("6433.57"), new BigDecimal("14")));
		return inss;
	}
	
	private List<Tariff> getIrrfTariffTable() {
		final List<Tariff> irrf = new ArrayList<>();
		irrf.add(new Tariff(new BigDecimal("1903.98"), new BigDecimal("0.000")));
		irrf.add(new Tariff(new BigDecimal("2826.68"), new BigDecimal("7.5")));
		irrf.add(new Tariff(new BigDecimal("3751.05"), new BigDecimal("15")));
		irrf.add(new Tariff(new BigDecimal("4664.68"), new BigDecimal("22.5")));
		irrf.add(new Tariff(null, new BigDecimal("27.5")));
		return irrf;
	}
	
}
