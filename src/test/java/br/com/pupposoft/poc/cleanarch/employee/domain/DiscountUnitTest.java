package br.com.pupposoft.poc.cleanarch.employee.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

public class DiscountUnitTest {
	
	@Test
	public void simpleDiscount() {
		final BigDecimal grossSalary = new BigDecimal("2000");
		final String name = "anyDiscount";
		final BigDecimal percent = new BigDecimal("5");
		
		final Discount discount = new Discount(name, percent);
		BigDecimal result = discount.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-100.00"), result);
	}

}
