package br.com.pupposoft.poc.cleanarch.employee.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

public class AliquotDiscountUnitTest {
	
	@Test
	public void aliquotDiscountRange1() {
		BigDecimal grossSalary = new BigDecimal("1000.00");
		List<Tariff> tariffs = Arrays.asList(
				new Tariff(new BigDecimal("1000.00"), new BigDecimal("2.00")), 
				new Tariff(null, new BigDecimal("3.00")));
		
		Discount aliquot = new AliquotDiscount("anyName", tariffs);
		
		BigDecimal result = aliquot.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-20.00000"), result);
		
	}
	
	@Test
	public void aliquotDiscountRange2() {
		BigDecimal grossSalary = new BigDecimal("2100.00");
		List<Tariff> tariffs = Arrays.asList(
				new Tariff(new BigDecimal("1000.00"), new BigDecimal("2.00")), 
				new Tariff(new BigDecimal("2100.00"), new BigDecimal("2.50")),
				new Tariff(null, new BigDecimal("3.00")));
		
		Discount aliquot = new AliquotDiscount("anyName", tariffs);
		
		BigDecimal result = aliquot.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-47.50000"), result);
		
	}
	
	@Test
	public void aliquotDiscountGrossLessThanRange2() {
		BigDecimal grossSalary = new BigDecimal("1800.00");
		List<Tariff> tariffs = Arrays.asList(
				new Tariff(new BigDecimal("1000.00"), new BigDecimal("2.00")), 
				new Tariff(new BigDecimal("2100.00"), new BigDecimal("2.50")),
				new Tariff(null, new BigDecimal("3.00")));
		
		Discount aliquot = new AliquotDiscount("anyName", tariffs);
		
		BigDecimal result = aliquot.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-40.00000"), result);
		
	}
	
	
	@Test
	public void aliquotDiscountRangeNull() {
		BigDecimal grossSalary = new BigDecimal("2500.00");
		List<Tariff> tariffs = Arrays.asList(
				new Tariff(new BigDecimal("1000.00"), new BigDecimal("2.00")), 
				new Tariff(new BigDecimal("2100.00"), new BigDecimal("2.50")),
				new Tariff(null, new BigDecimal("3.00")));
		
		Discount aliquot = new AliquotDiscount("anyName", tariffs);
		
		BigDecimal result = aliquot.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-59.50000"), result);
		
	}
	
	@Test
	public void inssCalcRange1() {
		final BigDecimal grossSalary = new BigDecimal("1100");
	
		final Discount aliquot = new AliquotDiscount("inss", getInssTariffTable());
	
		BigDecimal result = aliquot.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-82.500000"), result);
	}
	
	@Test
	public void inssCalcGrossLessThanRange2() {
		final BigDecimal grossSalary = new BigDecimal("1100.01");
	
		final Discount aliquot = new AliquotDiscount("inss", getInssTariffTable());
	
		BigDecimal result = aliquot.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-82.500900"), result);
	}
	
	@Test
	public void inssCalcRange2() {
		final BigDecimal grossSalary = new BigDecimal("2203.48");
	
		final Discount aliquot = new AliquotDiscount("inss", getInssTariffTable());
	
		BigDecimal result = aliquot.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-181.813200"), result);
	}
	
	@Test
	public void inssCalcGrossLessThanRange3() {
		final BigDecimal grossSalary = new BigDecimal("2203.49");
	
		final Discount aliquot = new AliquotDiscount("inss", getInssTariffTable());
	
		BigDecimal result = aliquot.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-181.814400"), result);
	}
	
	@Test
	public void inssCalcRange3() {
		final BigDecimal grossSalary = new BigDecimal("3305.22");
	
		final Discount aliquot = new AliquotDiscount("inss", getInssTariffTable());
	
		BigDecimal result = aliquot.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-314.022000"), result);
	}
	
	@Test
	public void inssCalcGrossLessThanRange4() {
		final BigDecimal grossSalary = new BigDecimal("3305.23");
	
		final Discount aliquot = new AliquotDiscount("inss", getInssTariffTable());
	
		BigDecimal result = aliquot.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-314.023400"), result);
	}
	
	@Test
	public void inssCalcRange4() {
		final BigDecimal grossSalary = new BigDecimal("6433.57");
	
		final Discount aliquot = new AliquotDiscount("inss", getInssTariffTable());
	
		BigDecimal result = aliquot.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-751.991000"), result);
	}
	
	@Test
	public void inssCalcGrossMoreThanRange4() {
		final BigDecimal grossSalary = new BigDecimal("7000.00");
	
		final Discount aliquot = new AliquotDiscount("inss", getInssTariffTable());
	
		BigDecimal result = aliquot.calculate(grossSalary);
		
		assertEquals(new BigDecimal("-751.991000"), result);
	}

	
	private List<Tariff> getInssTariffTable() {
		final List<Tariff> inss = new ArrayList<>();
		inss.add(new Tariff(new BigDecimal("1100.00"), new BigDecimal("7.5")));
		inss.add(new Tariff(new BigDecimal("2203.48"), new BigDecimal("9")));
		inss.add(new Tariff(new BigDecimal("3305.22"), new BigDecimal("12")));
		inss.add(new Tariff(new BigDecimal("6433.57"), new BigDecimal("14")));
		return inss;
	}


}
