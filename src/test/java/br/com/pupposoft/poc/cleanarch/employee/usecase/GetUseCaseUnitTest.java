package br.com.pupposoft.poc.cleanarch.employee.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.pupposoft.poc.cleanarch.employee.domain.Employee;
import br.com.pupposoft.poc.cleanarch.employee.exception.EmployeeNotFoundException;
import br.com.pupposoft.poc.cleanarch.employee.gateway.DataBaseGateway;
import static br.com.pupposoft.poc.test.util.databuilder.domain.EmployeeDataBuilder.*;

@ExtendWith(MockitoExtension.class)
public class GetUseCaseUnitTest {

	@InjectMocks
	private GetUseCase<String> getUseCase;
	
	@Mock
	private DataBaseGateway<String> dataBaseGateway;
	
	@Test
	public void getWithSuccess() {
		final String id = "anyId";
		final Employee employeeToBeReturn = getAny();
		
		final Optional<Employee> employeeOp = Optional.of(employeeToBeReturn); 
		doReturn(employeeOp).when(dataBaseGateway).findById(id);
		
		final Employee employeeFound = getUseCase.get(id);
		
		assertEquals(employeeToBeReturn, employeeFound);
	}

	@Test
	public void getWithEmployeeNotFound() {
		final String id = "anyId";
		
		final Optional<Employee> employeeOp = Optional.empty(); 
		doReturn(employeeOp).when(dataBaseGateway).findById(id);
		
		assertThrows(EmployeeNotFoundException.class, () -> {
			getUseCase.get(id);
		});
	}
	
	@Test
	public void getWithAllSuccess() {
		final List<Employee> employeeListToBeReturn = getAny(2);
		
		doReturn(employeeListToBeReturn).when(dataBaseGateway).getAll();
		
		final List<Employee> allEmployee = getUseCase.getAll();
		
		assertEquals(employeeListToBeReturn, allEmployee);
	}
	
}
