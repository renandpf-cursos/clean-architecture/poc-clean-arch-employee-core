package br.com.pupposoft.poc.test.util.databuilder.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.com.pupposoft.poc.cleanarch.employee.domain.Employee;
import br.com.pupposoft.poc.test.util.databuilder.DataBuilderBase;

public class EmployeeDataBuilder extends DataBuilderBase {

	public static Employee getAny() {
		return Employee
				.builder()
					.cpf(getNextRandomString())
					.firstName(getNextRandomString())
					.lastName(getNextRandomString())
					.birthDate(LocalDate.now())
					.salarys(new ArrayList<>())
				.build();
	}
	
	public static List<Employee> getAny(int number) {
		final List<Employee> employeeList = new ArrayList<>();
		
		for (int i = 0; i < number; i++) {
			employeeList.add(getAny());
		}
		
		return employeeList;
	}
	
}
