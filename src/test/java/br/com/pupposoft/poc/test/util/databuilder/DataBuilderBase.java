package br.com.pupposoft.poc.test.util.databuilder;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

public abstract class DataBuilderBase {
	private static final int AMOUNT_STRING_VALUES = 200;
	
	protected static String[] randomStringValues = null;
	private static void generateStringValues(final int size) {
		randomStringValues = new String[size];
		int length = 10;
	    boolean useLetters = true;
	    boolean useNumbers = true;
	    
	    for (int i = 0; i < randomStringValues.length; i++) {
	    	final String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
			randomStringValues[i] = generatedString;
		}
	}
	static {
		if(randomStringValues == null) {
			generateStringValues(AMOUNT_STRING_VALUES);
		}
	}
	private static int index = 0;
	public static String getNextRandomString() {
		final String next = randomStringValues[index++];
		
		if(index >= randomStringValues.length - 1) {
			index = 0;
		}
		
		return next;
	}
	
	public static int getRandomInt() {
	    final Random random = new Random();
	    return random.ints(0, 5000)
	      .findFirst()
	      .getAsInt();
	}
	
	public static long getRandomLong() {
	    return getRandomInt();
	}
	
}
